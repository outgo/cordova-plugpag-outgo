// Empty constructor
function PlugPag() {}

// The function that passes work along to native shells
// Message is a string, duration may be 'long' or 'short'
PlugPag.prototype.print = function(message, successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, 'PlugPagPrinter', 'print', [message]);
}

PlugPag.prototype.auth = function(successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, 'PlugPagPrinter', 'auth', []);
}

/**
params:
int paymentType (1: credito, 2: debito, 3: voucher),
int amountInCents,
int installmentType (1: a vista, 2: vendedor, 3: comprador),
int installmentCount,
string reference,
bool printReceipt
*/
PlugPag.prototype.purchase = function(params, successCallback, errorCallback) {
    var args = [];
    args.push(params.paymentType);
    args.push(params.amountInCents);
    args.push(params.installmentType);
    args.push(params.installmentCount);
    args.push(params.reference);
    args.push(params.printReceipt);

    cordova.exec(successCallback, errorCallback, 'PlugPagCardReader', 'purchase', args);
}

PlugPag.prototype.abort = function(successCallback, errorCallback) {
    cordova.exec(successCallback, errorCallback, 'PlugPagCardReader', 'abort', []);
}

// Installation constructor that binds PlugPag to window
PlugPag.install = function() {
  window.plugPag = new PlugPag();
  return window.plugPag;
};
cordova.addConstructor(PlugPag.install);