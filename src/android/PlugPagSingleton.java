package br.com.outgo.plugpagplugin;

import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPag;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagAppIdentification;

import android.util.Log;
import java.lang.reflect.Method;

import android.content.Context;

public class PlugPagSingleton {

    private static PlugPag _plugPag = null;

    public static PlugPag getPlugPag(Context context) {
        if (_plugPag == null) {
            // Cria a identificação do aplicativo
            PlugPagAppIdentification appIdentification = new PlugPagAppIdentification("Outgo", "6.2.21");
            // Cria a referência do PlugPag
            _plugPag = new PlugPag(context, appIdentification);
        }

        return _plugPag;
    }
}