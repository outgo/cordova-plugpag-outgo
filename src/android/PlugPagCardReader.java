package br.com.outgo.plugpagplugin;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import android.util.Log;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPag;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagAbortResult;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagActivationData;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagAppIdentification;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagInitializationResult;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagCustomPrinterLayout;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagPaymentData;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagPrintResult;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagPrinterListener;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagTransactionResult;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagVoidData;
import br.com.uol.pagseguro.plugpagservice.wrapper.exception.PlugPagException;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class PlugPagCardReader extends CordovaPlugin {

    private PlugPagPaymentData mPlugPagPaymentData = null;

    public PlugPag getPlugPag() {
        return PlugPagSingleton.getPlugPag(this.cordova.getActivity().getApplicationContext());
    }

    public PlugPagCardReader() { }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        try {
            switch (action) {
                case "purchase":
                    doPayment(new PlugPagPaymentData(
                        (int)args.get(0),
                        (int)args.get(1),
                        (int)args.get(2),
                        (int)args.get(3),
                        (String)args.get(4),
                        (boolean)args.get(5)
                    ))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<ActionResult>() {
                        @Override public void onError(Throwable e) {
                            Log.e("Outgo", "onError()", e);
                            String stacktrace = e.getMessage() + "\n\n";
                            for (StackTraceElement stackTraceElement : e.getStackTrace()) {
                                stacktrace += stackTraceElement.toString() + "\n";
                            }
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, stacktrace);
                            callbackContext.sendPluginResult(pluginResult);
                        }

                        @Override public void onNext(ActionResult result) {
                            Log.d("Outgo", "onNext(" + result.message + ")");

                            try {
                                JSONObject response = new JSONObject();
                                response.put("message", result.message);
                                response.put("code", result.eventCode);
                                response.put("transactionId", result.transactionId);
                                response.put("transactionCode", result.transactionCode);
                                response.put("result", result.result);
                                response.put("errorCode", result.errorCode);
                                response.put("serial", result.serial);

                                PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, response);
                                pluginResult.setKeepCallback(true);
                                callbackContext.sendPluginResult(pluginResult);

                            } catch (JSONException e) {
                                PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
                                callbackContext.sendPluginResult(pluginResult);
                            }
                        }

                        @Override public void onComplete() {}
                    });
                    break;
                case "abort":
                    doAbort()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<ActionResult>() {
                        @Override public void onError(Throwable e) {
                            Log.e("Outgo", "onError()", e);
                            String stacktrace = e.getMessage() + "\n\n";
                            for (StackTraceElement stackTraceElement : e.getStackTrace()) {
                                stacktrace += stackTraceElement.toString() + "\n";
                            }
                            PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, stacktrace);
                            callbackContext.sendPluginResult(pluginResult);
                        }

                        @Override public void onNext(ActionResult result) {
                            Log.d("Outgo", "onNext(" + result + ")");

                            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
                            callbackContext.sendPluginResult(pluginResult);
                        }

                        @Override public void onComplete() {}
                    });
                    break;
            }
        } catch (Exception exception) {

            String stacktrace = "";
            for (StackTraceElement stackTraceElement : exception.getStackTrace()) {
                stacktrace += stackTraceElement.toString();
            }
            PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, stacktrace);
            callbackContext.sendPluginResult(pluginResult);

        }

        return true;
    }


//    private enum PAYMENT_TYPE {
//        CREDITO = 1,
//        DEBITO = 2,
//        VOUCHER = 3
//    }
//    private enum INSTALLMENT_TYPE {
//        A_VISTA = 1,
//        PARC_VENDEDOR = 2,
//        PARC_COMPRADOR = 3
//    }

//    public Observable<ActionResult> doCreditPaymentWithSellerInstallments() {
//        return doPayment(new PlugPagPaymentData(
//                TYPE_CREDITO,
//                getAmount(),
//                INSTALLMENT_TYPE_PARC_VENDEDOR,
//                getInstallments(),
//                USER_REFERENCE,
//                true
//        ));
//    }

    public Observable<ActionResult> doRefundPayment(ActionResult actionResult) {
        if (actionResult.getTransactionCode() == null) {
            return Observable.error(new Exception("Nenhuma transação encontrada"));
        }
        return doRefund(new PlugPagVoidData(actionResult.getTransactionCode(), actionResult.getTransactionId(), true));
    }

    private Observable<ActionResult> doRefund(final PlugPagVoidData plugPagVoidData) {
        return Observable.create(emitter -> {
            ActionResult result = new ActionResult();
            setListener(emitter, result);
            setPrintListener(emitter, result);
            getPlugPag().setPlugPagCustomPrinterLayout(getCustomPrinterDialog());
            PlugPagTransactionResult plugPagTransactionResult = getPlugPag().voidPayment(plugPagVoidData);
            sendResponse(emitter, plugPagTransactionResult, result);
        });
    }

    private Observable<ActionResult> doPayment(final PlugPagPaymentData paymentData) {
        mPlugPagPaymentData = paymentData;
        return Observable.create(emitter -> {

            // PlugPagInitializationResult initResult = getPlugPag().initializeAndActivatePinpad(new PlugPagActivationData("403938"));
            PlugPagInitializationResult initResult = getPlugPag().initializeAndActivatePinpad(new PlugPagActivationData("974642"));
            if (initResult.getResult() == PlugPag.RET_OK) {

                getPlugPag().setPlugPagCustomPrinterLayout(getCustomPrinterDialog());
                ActionResult result = new ActionResult();
                setListener(emitter, result);
                setPrintListener(emitter, result);
                PlugPagTransactionResult plugPagTransactionResult = getPlugPag().doPayment(paymentData);
                sendResponse(emitter, plugPagTransactionResult, result);

            } else {
                emitter.onError(new PlugPagException(initResult.getErrorMessage(), initResult.getErrorCode()));
            }
                
        });
    }

    private Observable<ActionResult> doAbort() {
        return Observable.create(emitter -> {
            int abortResult = getPlugPag().abort().getResult();
            ActionResult result = new ActionResult();
            result.result = abortResult;
            emitter.onNext(result);
        });
    }

    private void sendResponse(final ObservableEmitter<ActionResult> emitter, PlugPagTransactionResult plugPagTransactionResult,
                              ActionResult result) {
        if (plugPagTransactionResult.getResult() != 0) {
            emitter.onError(new PlugPagException(plugPagTransactionResult.getMessage(), plugPagTransactionResult.getErrorCode()));
            emitter.onComplete();
        } else {
            result.setTransactionCode(plugPagTransactionResult.getTransactionCode());
            result.setTransactionId(plugPagTransactionResult.getTransactionId());
            result.setSerial(plugPagTransactionResult.getTerminalSerialNumber());
            emitter.onNext(result);

            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    emitter.onComplete();
                }
            }, 500L);
        }
    }

    private void sendResponse(ObservableEmitter<ActionResult> emitter, PlugPagPrintResult printResult,
                              ActionResult result) {

        if (printResult.getResult() != 0) {
            result.setResult(printResult.getResult());
        }
        emitter.onNext(result);
    }

    private void setListener(ObservableEmitter<ActionResult> emitter, ActionResult result) {
        getPlugPag().setEventListener(plugPagEventData -> {
            result.setEventCode(plugPagEventData.getEventCode());
            result.setMessage(plugPagEventData.getCustomMessage());
            emitter.onNext(result);
        });
    }

    private void setPrintListener(ObservableEmitter<ActionResult> emitter, ActionResult result) {
        getPlugPag().setPrinterListener(new PlugPagPrinterListener() {
            @Override
            public void onError(PlugPagPrintResult printResult) {
                result.setResult(printResult.getResult());
                result.setMessage(String.format("Error %s %s", printResult.getErrorCode(), printResult.getMessage()));
                result.setErrorCode(printResult.getErrorCode());
                emitter.onNext(result);
            }

            @Override
            public void onSuccess(PlugPagPrintResult printResult) {
                result.setResult(printResult.getResult());
                result.setMessage(String.format(Locale.getDefault(),"Print OK: Steps [%d]", printResult.getSteps()));
                result.setErrorCode(printResult.getErrorCode());
                emitter.onNext(result);
            }
        });
    }

    public PlugPagPaymentData getEventPaymentData(){
        return mPlugPagPaymentData;
    }

    public Observable<ActionResult> printStablishmentReceipt() {
        return Observable.create(emitter -> {
            ActionResult actionResult = new ActionResult();
            setPrintListener(emitter, actionResult);
            PlugPagPrintResult result = getPlugPag().reprintStablishmentReceipt();
            sendResponse(emitter, result, actionResult);
        });
    }

    public Observable<ActionResult> printCustomerReceipt() {
        return Observable.create(emitter -> {
            ActionResult actionResult = new ActionResult();
            setPrintListener(emitter, actionResult);
            PlugPagPrintResult result = getPlugPag().reprintCustomerReceipt();
            sendResponse(emitter, result, actionResult);
        });
    }

    public Observable<Object> abort() {
        return Observable.create(emitter -> {
            getPlugPag().abort();
            PlugPagAbortResult result = getPlugPag().abort();

            if (result.getResult() == 0) {
                emitter.onNext(new Object());
            } else {
                emitter.onError(new Exception("Erro ao abortar"));
            }

            emitter.onComplete();
        });
    }

    public Observable<ActionResult> getLastTransaction() {
        return Observable.create(emitter -> {
            ActionResult actionResult = new ActionResult();

            PlugPagTransactionResult result = getPlugPag().getLastApprovedTransaction();

            sendResponse(emitter, result, actionResult);
        });
    }

    public PlugPagCustomPrinterLayout getCustomPrinterDialog() {
        PlugPagCustomPrinterLayout customDialog = new PlugPagCustomPrinterLayout();
        customDialog.setTitle("Imprimir via do cliente?");
        customDialog.setButtonBackgroundColor("#D60F2A");
        return customDialog;
    }

    private class ActionResult {

        String transactionCode;
        String transactionId;
        String message;
        int eventCode;
        String errorCode;
        int result=0;
        String serial;

        public String getTransactionCode() {
            return transactionCode;
        }

        public void setTransactionCode(String transactionCode) {
            this.transactionCode = transactionCode;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getEventCode() {
            return eventCode;
        }

        public void setEventCode(int eventCode) {
            this.eventCode = eventCode;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public int getResult() {
            return result;
        }

        public void setResult(int result) {
            this.result = result;
        }

        public String getSerial() {
            return serial;
        }

        public void setSerial(String serial) {
            this.serial = serial;
        }
    }
}