package br.com.outgo.plugpagplugin;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import android.util.Base64;
import android.util.Log;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;

import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPag;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagActivationData;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagAppIdentification;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagInitializationResult;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagPrintResult;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagPrinterData;
import br.com.uol.pagseguro.plugpagservice.wrapper.PlugPagPrinterListener;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class PlugPagPrinter extends CordovaPlugin {

    public String img64;

    public PlugPag getPlugPag() {
        return PlugPagSingleton.getPlugPag(this.cordova.getActivity().getApplicationContext());
    }

    private static final int PERMISSIONS_REQUEST_CODE = 0x1234;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        try {
            PluginResult pluginResult;
            switch (action) {
                case "print":
                    img64 = args.get(0).toString();
                    new PrintFile().execute(callbackContext);
                    break;
                case "auth":
                    requestPermissions();
                    pluginResult = new PluginResult(PluginResult.Status.OK);
                    callbackContext.sendPluginResult(pluginResult);
                    break;
                default:
                    pluginResult = new PluginResult(PluginResult.Status.ERROR, "Função não cadastrada");
                    callbackContext.sendPluginResult(pluginResult);
            }
        } catch (Exception exception) {

            String stacktrace = "";
            for (StackTraceElement stackTraceElement : exception.getStackTrace()) {
                stacktrace += stackTraceElement.toString();
            }
            PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, stacktrace);
            callbackContext.sendPluginResult(pluginResult);

        }

        return true;
    }

    private class PrintFile extends AsyncTask<CallbackContext, Void, Boolean>{

        @Override
        protected Boolean doInBackground(CallbackContext... contexts) {

            CallbackContext callbackContext = contexts[0];
            try {
                if (img64 != null){
                    requestPermissions(); //Verifica permissões antes

                    byte[] decodedImg = Base64.decode(img64, Base64.DEFAULT);
                    FileOutputStream stream = new FileOutputStream("/sdcard/Download/print2.jpg");
                    stream.write(decodedImg);
                    stream.close();
                }

                if(!getPlugPag().isAuthenticated()){
                    getPlugPag().initializeAndActivatePinpad(new PlugPagActivationData("974642"));
                }

                File file = new File("/sdcard/Download/print2.jpg");
                if (file.exists()) {
                    String absolutePath = file.getAbsolutePath();
                    PlugPagPrinterData data = new PlugPagPrinterData(absolutePath, 1, 50);

                    // Imprime arquivo
                    PlugPagPrintResult result = getPlugPag().printFromFile(data);
                    file.delete();

                    PluginResult pluginResult;
                    if (result.getResult() == PlugPag.RET_OK) {

                        Log.d("SUCESSO", "IMPRIMIU");
                        pluginResult = new PluginResult(PluginResult.Status.OK);
                        callbackContext.sendPluginResult(pluginResult);

                        return  true;

                    } else {

                        try {
                            JSONObject errorObject = new JSONObject();
                            errorObject.put("message", result.getMessage()); // Mensagem de erro
                            errorObject.put("code", result.getErrorCode()); // Código de erro

                            pluginResult = new PluginResult(PluginResult.Status.ERROR, errorObject);
                        } catch (JSONException e) {
                            pluginResult = new PluginResult(PluginResult.Status.ERROR, "deu erro.");
                        }

                        callbackContext.sendPluginResult(pluginResult);
                        return false;

                    }
                } else {
                    PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, "Imagem não encontrada.");
                    callbackContext.sendPluginResult(pluginResult);

                    return false;
                }
            } catch (Exception exception) {
                String stacktrace = exception.getMessage() + "  -  " + img64;

//                for (StackTraceElement stackTraceElement : exception.getStackTrace()) {
//                    stacktrace += stackTraceElement.toString();
//                }

                PluginResult pluginResult = new PluginResult(PluginResult.Status.ERROR, stacktrace);
                callbackContext.sendPluginResult(pluginResult);
                return false;
            }
        }
    }


    //FUNCAO PARA SOLICITAR PERMISSÃO
    public void requestPermissions() {
        String[] missingPermissions = null;

        missingPermissions = this.getManifestPermissions();

        if (missingPermissions != null && missingPermissions.length > 0) {
            this.cordova.requestPermissions(this, PERMISSIONS_REQUEST_CODE, missingPermissions);
        } else {
            //showMessage("TODAS AS PERMISSOES DADAS");
        }
    }

    private String[] getManifestPermissions() {
        String[] permissions = null;
        PackageInfo info = null;

        try {
            info = this.cordova.getActivity().getApplicationContext().getPackageManager().getPackageInfo(this.cordova.getActivity().getApplicationContext().getPackageName(), PackageManager.GET_PERMISSIONS);
            permissions = info.requestedPermissions;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("SmartCoffee", "Package name not found", e);
        }

        if (permissions == null) {
            permissions = new String[0];
        }

        return permissions;
    }

}